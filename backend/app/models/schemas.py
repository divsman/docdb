from .. import db

"""
Define Aux Services Model
"""


class AuxServices(db.Model):
    __tablename__ = "aux_services"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    company_name = db.Column(db.String)
    address = db.Column(db.String)
    city = db.Column(db.String)
    state = db.Column(db.String)
    zipcode = db.Column(db.String)
    phone = db.Column(db.String)
    product = db.Column(db.String)
    image = db.Column(db.String)
    aux_location = db.relationship(
        "AuxServicesLocationMap", back_populates="aux_service"
    )


"""
Define AuxServicesLocationMap Model
"""


class AuxServicesLocationMap(db.Model):
    __tablename__ = "aux_services_location_map"
    address = db.Column(db.String)
    city = db.Column(db.String)
    state = db.Column(db.String)
    latitude = db.Column(db.String)
    longitude = db.Column(db.String)
    aux_service_id = db.Column(
        db.Integer, db.ForeignKey("aux_services.id"), primary_key=True
    )
    aux_service = db.relationship("AuxServices", back_populates="aux_location")


"""
Define Doctors Model
"""


class Doctors(db.Model):
    __tablename__ = "doctors"
    npi_number = db.Column(db.Integer, primary_key=True)
    enumeration_type = db.Column(db.String)
    full_name = db.Column(db.String)
    name = db.Column(db.String)
    gender = db.Column(db.String)
    last_updated = db.Column(db.DateTime)
    address = db.Column(db.String)
    city = db.Column(db.String)
    state = db.Column(db.String)
    zipcode = db.Column(db.String)
    phone_number = db.Column(db.String)
    fax_number = db.Column(db.String)
    specialities = db.Column(db.String)
    doc_location = db.relationship("DoctorsLocationMap", back_populates="doctor")


"""
Define DoctorsLocationMap Model
"""


class DoctorsLocationMap(db.Model):
    __tablename__ = "doctors_location_map"
    address = db.Column(db.String)
    city = db.Column(db.String)
    state = db.Column(db.String)
    latitude = db.Column(db.String)
    longitude = db.Column(db.String)
    doctor_id = db.Column(
        db.Integer, db.ForeignKey("doctors.npi_number"), primary_key=True
    )
    doctor = db.relationship("Doctors", back_populates="doc_location")


"""
Define Transit Connections Model  
"""


class TransitConnections(db.Model):
    __tablename__ = "transit_connections"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    aux_service_id = db.Column(db.Integer)
    doctor_id = db.Column(db.Integer)
    aux_latitude = db.Column(db.String)
    aux_longitude = db.Column(db.String)
    doctor_latitude = db.Column(db.String)
    doctor_longitude = db.Column(db.String)
    doctor_city = db.Column(db.String)
    aux_city = db.Column(db.String)
