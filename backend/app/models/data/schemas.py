from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship


"""
Defines the Schemas for the 5 different tables
"""

Base = declarative_base()

# Schema for the aux_services table
class AuxServices(Base):
    __tablename__ = "aux_services"
    id = Column(Integer, primary_key=True)
    company_name = Column(String)
    address = Column(String)
    city = Column(String)
    state = Column(String)
    zipcode = Column(String)
    phone = Column(String)
    product = Column(String)
    image = Column(String)
    # creates relationship to the aux_services_location_map table
    aux_location = relationship("AuxServicesLocationMap", back_populates="aux_service")

    def __repr__(self):
        return (
            "AuxServices: \n id = {}, CompanyName = {}, Address = {},"
            " City = {}, State = {}, Zipcode = {}, Phone = {},"
            " Product = {}, Image = {}".format(
                self.id,
                self.company_name,
                self.address,
                self.city,
                self.state,
                self.zipcode,
                self.phone,
                self.product,
                self.image,
            )
        )


# Schema for the doctors table
class Doctors(Base):
    __tablename__ = "doctors"
    npi_number = Column(Integer, primary_key=True)
    enumeration_type = Column(String)
    full_name = Column(String)
    name = Column(String)
    gender = Column(String)
    last_updated = Column(DateTime)
    address = Column(String)
    city = Column(String)
    state = Column(String)
    zipcode = Column(String)
    phone_number = Column(String)
    fax_number = Column(String)
    specialities = Column(String)
    # creates relationship to the doctors_location_map table
    doc_location = relationship("DoctorsLocationMap", back_populates="doctor")

    def __repr__(self):
        return (
            "Doctors: \n NPI Number = {}, Enumeration Type = {},"
            " Full Name = {} Name = {}, Gender = {}, Last Updated = {}, "
            "Addy = {}, City = {}, Zip Code = {}, Phone Number = {}, "
            "Fax Number = {}, Specialities = {}".format(
                self.npi_number,
                self.enumeration_type,
                self.full_name,
                self.name,
                self.gender,
                self.last_updated,
                self.address,
                self.city,
                self.zipcode,
                self.phone_number,
                self.fax_number,
                self.specialities,
            )
        )


# Schema for the doctors_location_map table
class DoctorsLocationMap(Base):
    __tablename__ = "doctors_location_map"
    address = Column(String)
    city = Column(String)
    state = Column(String)
    latitude = Column(String)
    longitude = Column(String)
    # creates relationship to the doctors table
    doctor_id = Column(Integer, ForeignKey("doctors.npi_number"), primary_key=True)
    doctor = relationship("Doctors", back_populates="doc_location")

    def __repr__(self):
        return (
            "LocationMap: \n id = {}, addy = {}, city = {}, state = {},"
            " lat = {}, long = {}".format(
                self.id,
                self.address,
                self.city,
                self.state,
                self.latitude,
                self.longitude,
            )
        )


# Schema for the aux_services_location_map table
class AuxServicesLocationMap(Base):
    __tablename__ = "aux_services_location_map"
    address = Column(String)
    city = Column(String)
    state = Column(String)
    latitude = Column(String)
    longitude = Column(String)
    # creates relationship to the aux_service table
    aux_service_id = Column(Integer, ForeignKey("aux_services.id"), primary_key=True)
    aux_service = relationship("AuxServices", back_populates="aux_location")

    def __repr__(self):
        return (
            "LocationMap: \n id = {}, addy = {}, city = {}, state = {}, "
            "lat = {}, long = {}".format(
                self.id,
                self.address,
                self.city,
                self.state,
                self.latitude,
                self.longitude,
            )
        )


# table contains info about transits and their aux services/doctor coordinates
class TransitConnections(Base):
    __tablename__ = "transit_connections"
    id = Column(Integer, primary_key=True, autoincrement=True)
    aux_service_id = Column(Integer)
    doctor_id = Column(Integer)
    aux_latitude = Column(String)
    aux_longitude = Column(String)
    aux_city = Column(String)
    doctor_latitude = Column(String)
    doctor_longitude = Column(String)
    doctor_city = Column(String)

    def __repr__(self):
        return (
            "TransitConnections: \n id = {}, aux_service_id = {}, "
            "doctor_id = {}, aux_lat = {}, aux_long = {}, doc_lat = {},"
            " doc_long = {}".format(
                self.id,
                self.aux_service_id,
                self.doctor_id,
                self.aux_latitude,
                self.aux_longitude,
                self.doctor_latitude,
                self.doctor_longitude,
            )
        )
