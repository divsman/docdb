# helper function to make_list out of sql objects
from .schemas import AuxServices, AuxServicesLocationMap, Doctors, DoctorsLocationMap
from .. import db


def make_list(all_items, location_type):
    ret = []
    for item, location in all_items:
        # make dict
        formatted_dict = item.__dict__
        location_dict = location.__dict__

        # remove the object id
        formatted_dict.pop("_sa_instance_state", None)
        location_dict.pop("_sa_instance_state", None)

        # add sub dict
        formatted_dict.update({location_type: [location_dict]})

        ret.append(formatted_dict)
    return ret


# helper function to make doctor query
def make_doctor_query(val, descending=False):
    if descending:
        if val == "city":
            query = (
                db.session.query(Doctors, DoctorsLocationMap)
                .join(
                    DoctorsLocationMap,
                    DoctorsLocationMap.doctor_id == Doctors.npi_number,
                )
                .order_by(Doctors.city.desc())
            )
        elif val == "last_updated":
            query = (
                db.session.query(Doctors, DoctorsLocationMap)
                .join(
                    DoctorsLocationMap,
                    DoctorsLocationMap.doctor_id == Doctors.npi_number,
                )
                .order_by(Doctors.last_updated.desc())
            )
        else:
            query = (
                db.session.query(Doctors, DoctorsLocationMap)
                .join(
                    DoctorsLocationMap,
                    DoctorsLocationMap.doctor_id == Doctors.npi_number,
                )
                .order_by(Doctors.full_name.desc())
            )
    else:
        if val == "city":
            query = (
                db.session.query(Doctors, DoctorsLocationMap)
                .join(
                    DoctorsLocationMap,
                    DoctorsLocationMap.doctor_id == Doctors.npi_number,
                )
                .order_by(Doctors.city.asc())
            )
        elif val == "last_updated":
            query = (
                db.session.query(Doctors, DoctorsLocationMap)
                .join(
                    DoctorsLocationMap,
                    DoctorsLocationMap.doctor_id == Doctors.npi_number,
                )
                .order_by(Doctors.last_updated.asc())
            )
        else:
            query = (
                db.session.query(Doctors, DoctorsLocationMap)
                .join(
                    DoctorsLocationMap,
                    DoctorsLocationMap.doctor_id == Doctors.npi_number,
                )
                .order_by(Doctors.full_name.asc())
            )
    return query


# helper function to make aux query
def make_aux_service_query(val, descending=False):
    if descending:
        if val == "city":
            query = (
                db.session.query(AuxServices, AuxServicesLocationMap)
                .join(
                    AuxServicesLocationMap,
                    AuxServicesLocationMap.aux_service_id == AuxServices.id,
                )
                .order_by(AuxServices.city.desc())
            )
        else:
            query = (
                db.session.query(AuxServices, AuxServicesLocationMap)
                .join(
                    AuxServicesLocationMap,
                    AuxServicesLocationMap.aux_service_id == AuxServices.id,
                )
                .order_by(AuxServices.company_name.desc())
            )
    else:
        if val == "city":
            query = (
                db.session.query(AuxServices, AuxServicesLocationMap)
                .join(
                    AuxServicesLocationMap,
                    AuxServicesLocationMap.aux_service_id == AuxServices.id,
                )
                .order_by(AuxServices.city.asc())
            )
        else:
            query = (
                db.session.query(AuxServices, AuxServicesLocationMap)
                .join(
                    AuxServicesLocationMap,
                    AuxServicesLocationMap.aux_service_id == AuxServices.id,
                )
                .order_by(AuxServices.company_name.asc())
            )
    return query
