from algoliasearch.search_client import SearchClient
from .algolia_helper import (
    clean_doctor,
    get_aux_service,
    get_doctor,
    TransitConnections,
)
import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def build_transit_list(endpoint):
    # get transit info
    all_connections = []

    response = requests.request("GET", endpoint + "transit_connections")

    print("Getting all Transit Connections, this will take some time")
    # since this is paginated
    while True:
        # deal with the first response and then work with the rest
        r = response.json()
        batch_connections = r["objects"]

        # looping thru the list and adding the connections to all connects
        for connection in batch_connections:
            # adding doctor
            doc_response = get_doctor(connection["doctor_id"], endpoint)
            connection["doctor"] = [doc_response]

            # add doctor name
            connection["doctor_name"] = doc_response["full_name"]

            # adding aux_response
            aux_response = get_aux_service(connection["aux_service_id"], endpoint)
            connection["aux_service"] = [aux_response]

            # add company name
            connection["aux_company_name"] = aux_response["company_name"]

            all_connections.append(connection)

        if r["page"] == r["total_pages"]:
            break

        response = requests.request(
            "GET",
            endpoint + "transit_connections?page={}".format(r["page"] + 1),
        )

    return all_connections


def build_doc_list(login, endpoint):

    # create engine and session for Postgres
    engine = create_engine(login)
    Session = sessionmaker(bind=engine)

    # get docs from endpoint
    all_docs = []
    response = requests.request("GET", endpoint + "doctors")

    # create session
    my_session = Session()

    print("Getting all Doctors, this will take some time")
    while True:
        # deal with the first response and then work with the rest
        r = response.json()
        batch_doctors = r["objects"]

        # looping thru the list and adding the connections to all connects
        for doctor in batch_doctors:

            # set objectID
            doctor["objectID"] = int(doctor["npi_number"])

            # pull transit connection row that links to the doctor npi
            trans_query = my_session.query(TransitConnections).filter(
                TransitConnections.doctor_id == doctor["npi_number"]
            )
            trans_con = trans_query.first()
            trans_con = trans_con.__dict__
            trans_con.pop("_sa_instance_state", None)
            doctor["transit_connection"] = [trans_con]

            # pull the aux column from the transit connection row and pull the aux row
            aux_response = get_aux_service(trans_con["aux_service_id"], endpoint)
            doctor["aux_service"] = [aux_response]

            # clean doctor
            clean_doctor(doctor)

            all_docs.append(doctor)

        if r["page"] == r["total_pages"]:
            break

        response = requests.request(
            "GET", endpoint + "doctors?page={}".format(r["page"] + 1)
        )

    return all_docs


def build_aux_list(login, endpoint):

    # create engine and session for Postgres
    engine = create_engine(login)
    Session = sessionmaker(bind=engine)

    # get aux_services
    all_aux_services = []

    response = requests.request("GET", endpoint + "aux_services")

    # create session
    my_session = Session()

    print("Getting all Aux Services, this will take some time")
    while True:
        # deal with the first response and then work with the rest
        r = response.json()
        batch_aux_services = r["objects"]

        # looping thru the list and adding the connections to all connects
        for aux_service in batch_aux_services:

            # set objectID
            aux_service["objectID"] = int(aux_service["id"])

            # pull transit connection row that links to the doctor npi
            trans_query = my_session.query(TransitConnections).filter(
                TransitConnections.aux_service_id == aux_service["id"]
            )
            trans_con = trans_query.first()
            trans_con = trans_con.__dict__
            trans_con.pop("_sa_instance_state", None)
            aux_service["transit_connection"] = [trans_con]

            # pull the aux column from the transit connection row and pull the aux row
            doc_response = get_doctor(trans_con["doctor_id"], endpoint)
            aux_service["doctor"] = [doc_response]

            all_aux_services.append(aux_service)

        if r["page"] == r["total_pages"]:
            break

        response = requests.request(
            "GET", endpoint + "aux_services?page={}".format(r["page"] + 1)
        )

    return all_aux_services


def create_total_index(
    login,
    app_id,
    api_key,
    endpoint,
    settings,
    update_settings=False,
    drop_index=False,
    fill=False,
):
    # create index
    client = SearchClient.create(app_id, api_key)  # create index run this once
    index = client.init_index("total_table")

    # drop table
    if drop_index:
        print("Dropping Index")
        index.delete()

    if fill:
        all_aux_services = build_aux_list(login, endpoint)
        all_docs = build_doc_list(login, endpoint)
        all_trans = build_transit_list(endpoint)

        index.save_objects(all_trans, {"autoGenerateObjectIDIfNotExist": True})
        index.save_objects(all_docs, {"autoGenerateObjectIDIfNotExist": True})
        index.save_objects(all_aux_services, {"autoGenerateObjectIDIfNotExist": True})

    # update settings
    if fill or update_settings:
        print("Setting Index settings for Total_tables Index")
        index.set_settings(settings)
