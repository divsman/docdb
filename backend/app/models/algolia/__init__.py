import json
from .main_indices import fill_doc_index, fill_transit_index, fill_aux_index
from .total_index import create_total_index
from .big_index import create_big_index


def get_params(index_name):
    # get params for the functions
    drop_index = input("Do you want to drop the {} index? ".format(index_name))
    fill = input(
        "Do you want to fill the {} index, this will drop the index? ".format(
            index_name
        )
    )
    update_settings = input(
        "Do you want to update settings for {} index? ".format(index_name)
    )

    drop_index = True if drop_index.lower() in ["yes", "y", "t", "true"] else False
    fill = True if fill.lower() in ["yes", "y", "t", "true"] else False
    update_settings = (
        True if update_settings.lower() in ["yes", "y", "t", "true"] else False
    )

    return drop_index, fill, update_settings


def open_file(file_name):
    # checking if property file exists and opening it
    try:
        return json.load(open(file_name))
    except:
        print("No file {}".format(file_name))
        return None


def get_confirmation(index_name):
    ret = input("Work with {} index? ".format(index_name))
    return True if ret.lower() in ["yes", "y", "t", "true"] else False


def main(doctors=False, aux_services=False, transit=False, total=False, big=False):
    if not doctors and not aux_services and not transit and not total and not big:
        print("Run again and please specify which index to create")

    values = open_file("algolia.json")
    doc_settings = open_file("doctor_index.json")
    transit_settings = open_file("transit_index.json")
    aux_settings = open_file("aux_services_index.json")
    total_settings = open_file("total_index.json")
    big_settings = open_file("big_index.json")

    if (
        not values
        or not doc_settings
        or not transit_settings
        or not aux_settings
        or not total_settings
        or not big_settings
    ):
        return

    # running doctors
    if doctors:
        drop_index, fill, update_settings = get_params("doctors")
        fill_doc_index(
            values["login"],
            values["app_id"],
            values["api_key"],
            values["endpoint"],
            doc_settings,
            update_settings=update_settings,
            drop_index=drop_index,
            fill=fill,
        )

    # running transit
    if transit:
        drop_index, fill, update_settings = get_params("transit")
        fill_transit_index(
            values["app_id"],
            values["api_key"],
            values["endpoint"],
            transit_settings,
            update_settings=update_settings,
            drop_index=drop_index,
            fill=fill,
        )

    # running aux_services
    if aux_services:
        drop_index, fill, update_settings = get_params("Aux Services")
        fill_aux_index(
            values["login"],
            values["app_id"],
            values["api_key"],
            values["endpoint"],
            aux_settings,
            update_settings=update_settings,
            drop_index=drop_index,
            fill=fill,
        )

    # running the total index
    if total:
        drop_index, fill, update_settings = get_params("total")
        create_total_index(
            values["login"],
            values["app_id"],
            values["api_key"],
            values["endpoint"],
            total_settings,
            update_settings=update_settings,
            drop_index=drop_index,
            fill=fill,
        )

    # running the big index
    if big:
        drop_index, fill, update_settings = get_params("big")
        create_big_index(
            values["app_id"],
            values["api_key"],
            values["endpoint"],
            total_settings,
            update_settings=update_settings,
            drop_index=drop_index,
            fill=fill,
        )


if __name__ == "__main__":
    print("**** ARE YOU RUNNING YOUR SERVER????? ****")
    doctors = get_confirmation("doctors")
    aux = get_confirmation("aux_services")
    transit = get_confirmation("transit_connections")
    total = get_confirmation("total_table")
    big = get_confirmation("big_table")
    main(doctors=doctors, aux_services=aux, transit=transit, total=total, big=big)
