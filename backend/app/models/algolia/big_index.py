from algoliasearch.search_client import SearchClient
from .algolia_helper import get_aux_service, get_doctor
import requests


def create_big_index(
    app_id,
    api_key,
    endpoint,
    settings,
    update_settings=False,
    drop_index=False,
    fill=False,
):

    # create the client
    client = SearchClient.create(app_id, api_key)
    # create index run this once
    index = client.init_index("big_table")

    # drop table
    if drop_index:
        print("Dropping Index")
        index.delete()

    everything = []

    # transit
    response = requests.request("GET", endpoint + "transit_connections")
    while True:
        response = response.json()
        batch_connections = response["objects"]
        for connection in batch_connections:
            # adding doctor
            doc_response = get_doctor(connection["doctor_id"], endpoint)
            connection["doctor"] = [doc_response]

            # add doctor name
            connection["doctor_name"] = doc_response["full_name"]

            # adding aux_response
            aux_response = get_aux_service(connection["aux_service_id"], endpoint)
            connection["aux_service"] = [aux_response]

            # add company name
            connection["aux_company_name"] = aux_response["company_name"]

            everything.append(connection)

        if response["page"] == response["total_pages"]:
            break

        response = requests.request(
            "GET",
            endpoint + "transit_connections?page={}".format(response["page"] + 1),
        )

    # doctors
    response = requests.request("GET", endpoint + "doctors")
    while True:
        response = response.json()
        batch_connections = response["objects"]
        for connection in batch_connections:
            everything.append(connection)

        if response["page"] == response["total_pages"]:
            break

        response = requests.request(
            "GET",
            endpoint + "doctors?page={}".format(response["page"] + 1),
        )

    # aux_services
    response = requests.request("GET", endpoint + "aux_services")
    while True:
        response = response.json()
        batch_connections = response["objects"]
        for connection in batch_connections:
            everything.append(connection)

        if response["page"] == response["total_pages"]:
            break

        response = requests.request(
            "GET",
            endpoint + "aux_services?page={}".format(response["page"] + 1),
        )

    # print(len(everything))
    if fill:
        index.save_objects(everything, {"autoGenerateObjectIDIfNotExist": True})

    # update settings
    if fill or update_settings:
        print("Setting Index settings for Big Index")
        index.set_settings(settings)
