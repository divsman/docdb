import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.firefox.options import Options
from time import sleep
import requests

class Selenium(unittest.TestCase):
    
    def setUp(self):
        setupDone = getattr(self, 'setupDone', False)
        if setupDone:
            return
        
        options = Options()
        options.headless = True
        self.driver = webdriver.Firefox(options=options)
        self.setupDone = True

    def test1(self):
        self.driver.get('https://www.mymedtro.com')
        result = self.driver.find_element_by_css_selector('.App')
        self.assertNotEqual(result, None)
        
    def test2(self):
        self.driver.get('https://www.mymedtro.com')
        result = self.driver.find_element_by_css_selector('#root')
        self.assertNotEqual(result, None)

    def test3(self):
        self.driver.get('https://www.mymedtro.com/doctors/')
        WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'DocCard')))
        result = self.driver.find_elements_by_class_name('DocCard')
        self.assertEqual(len(result), 10)
        
    # def test4(self):
    #     self.driver.get('https://www.mymedtro.com/transit/')
    #     WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.CSS_SELECTOR, 'tr')))
    #     result = self.driver.find_elements_by_css_selector('tr')
    #     self.assertEqual(len(result), 1)
        
    def test5(self):
        self.driver.get('https://www.mymedtro.com/aux_services/')
        WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'Link')))
        result = self.driver.find_elements_by_class_name('Link')
        self.assertEqual(len(result), 10)
        
    def test6(self):
        self.driver.get('https://www.mymedtro.com/about/')
        WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'about_card')))
        result = self.driver.find_elements_by_class_name('about_card')
        self.assertEqual(len(result), 5)
        
    # def test7(self):
    #     self.driver.get('https://www.mymedtro.com/')
    #     WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'nav-item')))
    #     result = self.driver.find_elements_by_class_name('nav-item')
    #     self.assertEqual(len(result), 6)

    def test8(self):
        self.driver.get('https://www.mymedtro.com/')
        WebDriverWait(self.driver, 4).until(ec.presence_of_all_elements_located((By.CLASS_NAME, 'btn-responsive')))
        result = self.driver.find_elements_by_class_name('btn-responsive')
        self.assertEqual(len(result), 3)

if __name__ == '__main__':
    unittest.main()