
import React from 'react'
import Map from './Map'

import { mount, render, shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('Map component test suite', () => {
    configure({ adapter: new Adapter() })
    it('tests for correct properties', () => {
        let map = render(<Map />).toArray()[0]
        expect(map.tagName).toBe('div')
    })
})