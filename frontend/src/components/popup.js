import React from 'react';
import './popup_styles.css';
import Button from '@material-ui/core/Button';
import AuxCard from '../pages/AuxServices/aux_card.js'
import TransitCard from '../pages/Transit/transit_card.js'
import DoctorCard from '../pages/Doctors/doctor_card.js'
import { CardGroup, Row } from 'react-bootstrap'

class Popup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            card: AuxCard,
        }
    }

    UNSAFE_componentWillMount() {
        if (this.props.type === 'doctor') {
            this.setState({ card: DoctorCard })
        }
        if (this.props.type === 'transit') {
            this.setState({ card: TransitCard })
        }
    }

    render() {
        return (
            <div className='popup'>
                <CardGroup>
                    <Row className="justify-content-md-center">
                        {(
                            (this.props.items).map((item) => (
                                <this.state.card key={item} id={item} compare={false} />
                            )))}
                    </Row>
                </CardGroup>
                <Button variant="contained" className='compareButton' onClick={this.props.closePopup}>Close</Button>
            </div>
        )
    }
}

export default Popup;