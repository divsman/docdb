import React from 'react'
import './home_styles.css'
import { Container, CardGroup, Row } from 'react-bootstrap'
import AuxCard from './../AuxServices/aux_card.js'
import TransitCard from './../Transit/transit_card.js'
import DoctorCard from './../Doctors/doctor_card.js'
import { Pagination } from '@material-ui/lab'
import { searchClient } from './../../components/SearchClient.js'
import algoliaImg from './../../components/algolia.svg'

class SearchPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            page: 1,
            total: 0,
            pages: 0,
            results: []
        }
    }

    componentDidMount() {
        this.setState({ search: this.props.location.state.search }, () => this.getResults());
    }

    getResults() {
        let strlst = []
        if (this.state.search) {
            strlst.push(this.state.search)
        }

        let index = searchClient.initIndex('big_table')
        const str = strlst.join()

        // algolia zero indexes pages
        const cur_page = this.state.page - 1
        index.search(str, { page: cur_page })
            .then(result => this.setState({ results: result.hits, total: result.nbHits, pages: result.nbPages, page: result.page + 1 }))
    }

    handleSearch(event) {
        this.setState({ page: 1, search: event.target.value }, () => this.getResults())
    }

    // pagination
    handlePag(event, page) {
        this.setState({ page: page }, () => this.getResults())
    }

    render() {
        return (
            <Container>
                {/* search bar */}
                <input className="input_style" type="text" value={this.state.search} onChange={this.handleSearch.bind(this)} />
                <img src={algoliaImg} alt='algolia icon' />
                <CardGroup>
                    <Row className="justify-content-md-center">

                        {/* display the correct card for each result */}
                        {this.state.results.map((res) => (
                            res.company_name ?
                                <AuxCard key={res.id} id={res.id} search={this.state.search} /> :
                                res.npi_number ?
                                    <DoctorCard key={res.npi_number} id={res.npi_number} search={this.state.search} /> :
                                    res.id ?
                                        <TransitCard key={res.id} id={res.id} search={this.state.search} /> :
                                        null
                        ))}
                    </Row>
                </CardGroup>

                {/* pagination */}
                <Row className="justify-content-md-center">
                    <Pagination count={this.state.pages} page={this.state.page} onChange={this.handlePag.bind(this)} />
                </Row>
                <br />
                <div>Total Instances: {this.state.total}</div>
                <br /><br />
            </Container>)
    }
}

export default SearchPage