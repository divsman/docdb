export const formatAddress = (doc) => {
    return doc.address + '\n' + doc.city + ', ' + doc.state + ' ' + doc.zipcode
}

export const mapGender = (gender) => {
    return gender === 'M' ? 'Male' : 'Female'
}

export const formatStatus = (status) =>{
    return status === 'A' ? 'Active' : 'Inactive'
}
