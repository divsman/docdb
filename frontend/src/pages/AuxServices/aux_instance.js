import React from 'react'
import { Container, CardGroup } from 'react-bootstrap'
import { formatProducts, formatAddress } from './aux_utils'
import GoogleApiWrapper from './../../components/Map.js'
import DoctorCard from './../Doctors/doctor_card.js'
import TransitCard from './../Transit/transit_card.js'
import { searchClient } from './../../components/SearchClient.js'
import './../Styles/mod_styles.css'

const index = searchClient.initIndex('aux_services')

class AuxInstance extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            place: [],
            doc: [],
            image: null,
            transit_id: null,
            doc_npi: null
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params

        // grab the correct doctor and transit
        index.search(id)
            .then(results => this.getConnections(results.hits[0]))

        const req_str = '/api/aux_services/' + String(id)
        fetch(req_str)
            .then(res => res.json())
            .then(data => this.setPlace(data))
    }

    getConnections(res) {
        const npi = res.doctor[0].doc_location[0].doctor_id
        const transit_id = res.transit_connection[0].id
        this.setState({ transit_id: transit_id, doc_npi: npi })
    }

    setPlace(data) {
        fetch(`https://maps.googleapis.com/maps/api/streetview?size=300x300&location=${data.address}&key=AIzaSyA2WpzkLDlb7eLIRQ29imgybOIJ9Vr6zlE`)
            .then(res => this.setState({ image: res["url"] }))
        this.setState({ place: data })

    }

    render() {
        const place = this.state.place
        const products = place.product ? formatProducts(place.product) : []
        const address = formatAddress(place)

        const lat = place.aux_location ? parseFloat(place.aux_location[0].latitude) : null
        const long = place.aux_location ? parseFloat(place.aux_location[0].longitude) : null

        return place ?
            <div>
                <div className="Instance">
                    <div className='row' style={{ minHeight: '300px' }}>
                        <div className='info'>
                            <h3>{place.company_name}</h3>
                            <br />
                            <h5>Products: {products}</h5>
                            <h5>Address: </h5>
                            <Container style={{ marginLeft: '20px' }}>{address.split('\n').map((item, i) => <h5 key={i}>{item}</h5>)}</Container>
                            <h5>Phone: {place.phone}</h5>
                        </div>
                        <GoogleApiWrapper key={place.company_name} lat={lat} long={long} />
                    </div>
                    <br /><br /><br />

                    <div className='row'>
                        <div className='col'>
                            <h4>Location</h4>
                            <div>
                                <img src={this.state.image} alt={`street view for ${place.company_name}`} />
                            </div>
                        </div>

                        <br /><br />

                        <div className='col'>
                            <h4>Related Doctor and Transit Route</h4>
                            <CardGroup>
                                {this.state.doc_npi ?
                                    <DoctorCard id={this.state.doc_npi} /> : <div>loading</div>}
                                {this.state.transit_id ?
                                    <TransitCard id={this.state.transit_id} /> : <div>loading</div>}
                            </CardGroup>
                        </div>
                        <br /><br />
                    </div>
                </div>
            </div>
            : <h1>Error, doctor not found</h1>
    }
}

export default AuxInstance