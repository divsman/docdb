
import React from 'react'
import Places from './aux_services'

import { render, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('aux_services.test.js test suite', () => {
    configure({ adapter: new Adapter() })
    let auxServices = render(<Places />)
    it('tests for correct rendering', () => {
        expect(auxServices.is('div')).toBe(true)
        expect(auxServices.find('h1').length).toBe(1)
        expect(auxServices.find('.fluid').length).toBe(1)
        expect(auxServices.find('.justify-content-md-center').length).toBe(2)
    })
})