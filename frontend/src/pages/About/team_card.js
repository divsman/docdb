import React from 'react'
import Card from 'react-bootstrap/Card'
import './../Styles/about_styles.css'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ShareIcon from '@material-ui/icons/Share';

class TeamCard extends React.Component {
    render() {
        const member = this.props.member
        const role = (member.leader) ? `${member.role} • Team Leader` : member.role 
        return (
            <Card className="about_card">
            <Card.Img variant="top" src={member.pic} style={{height:'320px'}}/>
            <Card.Body>
              <Card.Title>{member.name}</Card.Title>
              {role} <br/><br/>
              <Card.Text>
                {member.bio} <br/><br/>
                {member.jobs} <br/><br/>
                <span className='statBox'>
                  <span className='statItem'>
                    Commits <ShareIcon/> {member.commits}
                  </span>
                  <span className='statItem'>
                    Issues <ListAltIcon/> {member.issues}
                  </span>
                  <span className='statItem'>
                    Tests <CheckCircleOutlineIcon/> {member.tests}
                  </span>
                </span>
              </Card.Text>
            </Card.Body>
          </Card>
        )
    }
}

export default TeamCard