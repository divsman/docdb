import React from 'react'
import { Container, Card } from 'react-bootstrap'
import './../Styles/about_styles.css'

class ToolCard extends React.Component {
    render() {
        const tool = this.props.tool
        return (
            <a className="Link" key={tool.name} href={tool.link}>
                <Card className="tools">
                    <br />
                    <Container className="toolPicContainer">
                        <Card.Img variant="top" src={tool.image} className="toolpic" />
                    </Container>
                    <Card.Body>
                        <Card.Title>{tool.name}</Card.Title>
                        <Card.Text>
                            {tool.descr}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </a> )
    }
}

export default ToolCard