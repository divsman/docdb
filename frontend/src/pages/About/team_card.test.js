
import React from 'react'
import TeamCard from './team_card'
import duncanImg from './Images/duncan.jpg'

import { render, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('TeamCard component test suite', () => {
    configure({ adapter: new Adapter() })
    let member = {
        name: 'Duncan Huntsinger',
        pic: duncanImg,
        role: 'Backend Developer',
        jobs: 'Toolchains, deployment, and backend testing',
        bio: 'I’m a third year CS major from Schertz, Texas. In my free time I enjoy watching true crime web content, playing Factorio, listening to and playing music, doing Chinese/English language exchange, as well as watching films. I also have a teacup chihuahua named Pilgor.\n',
        commits: 0,
        issues: 0,
        tests: 0

    }
    let teamCard = render(<TeamCard key={member.name} member={member}/>)
    it('tests for correct properties', () => {
        expect(teamCard.is('.about_card')).toBe(true)
    })
})