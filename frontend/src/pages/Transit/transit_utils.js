export const formatTime = (datetime) => {
    if (datetime === null)
        return 'No Current Routes'
    // grab the chunks we care about
    const time = datetime.split('T')[1].split('-')[0]
    const hms = time.split(':')
    let h = parseInt(hms[0])
    const m = hms[1]
    let t = 'PM'

    // determine AM vs PM
    if (h <= 11)
        t = 'AM'

    // convert hours to central
    h = h % 12
    h = (h === 0) ? 12 : h

    // slap it all together
    return h + ':' + m + t
}

export const getElapsedTime = (ar_time, dep_time) => {
    // grab the chunks we care about
    const atime = ar_time.split('T')[1].split('-')[0].split(':')
    const dtime = dep_time.split('T')[1].split('-')[0].split(':')

    const h = (atime[0] - dtime[0] + 12) % 12
    const m = (atime[1] - dtime[1] + 60) % 60

    return h + 'h ' + m + 'm'
}