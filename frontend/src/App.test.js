
import React from 'react'
import App from './App'

import { render, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('App root component test suite', () => {
    configure({ adapter: new Adapter() })
    it('tests if the rendered app has the correct properties', () => {
        expect(render(<App />).is('.App')).toBe(true)
    })
})